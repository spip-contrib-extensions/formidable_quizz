<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans svn://zone.spip.org/spip-zone/_plugins_/formidable/trunk/lang/
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'config_bareme_explication' => 'Liste des réponses possibles et les points associés. Une réponse par ligne sous la forme "valeur|N" où N est un entier. Vous pouvez associer des nombres négatifs aussi. Si une réponse n’a pas de point, elle vaudra 0 par défaut.',
	'config_bareme_label' => 'Barème des points',
	'config_quizz_label' => 'Quizz',
	
	'resultats_score_label' => 'Score',
	'resultats_total_label' => 'Total',
);
